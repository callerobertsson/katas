# String Calculator Kata
#   http://craftsmanship.sv.cmu.edu/katas/string-calculator
#   Implemntation of step 1-6
# by calle@upset.se

exports.add = (numberString) ->

	if numberString is ''
		0

	else
		separator = /[\s,]/g

		if (numberString.indexOf '//') is 0
			separator = numberString[2]
			numberString = numberString[3..]
			
		numberArray = numberString.split separator

		for num, i in numberArray

			numberArray[i] = parseInt numberArray[i]

			if numberArray[i] < 0
				throw new Error "negatives not allowed"

			if numberArray[i] > 1000
				numberArray.splice(i, 1)
				console.log numberArray

		if numberArray.length is 1
			numberArray[0]

		else
			numberArray.reduce (a, b) -> 
				a + b
