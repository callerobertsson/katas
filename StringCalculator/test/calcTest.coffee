chai = require 'chai'
chai.should()

Calc = require '../src/calc'

describe 'Calc add', ->
	
	it 'should return 0 for empty input', ->
		(Calc.add '').should.equal 0
	
	it 'should return 4 for "4"', ->
		(Calc.add '4').should.equal 4

	it 'should return 17 for "8\\n9"', ->
		(Calc.add '8\n9').should.equal(17)

	it 'should return 99 for "1\\n2,3\\n93"', ->
		(Calc.add '1\n2,3\n93').should.equal(99)

	it 'should return 99 for "//|12|3|93"', ->
		(Calc.add '//|1|2|3|93').should.equal(99)

	it 'should throw on negative numbers', ->
		(-> Calc.add '1,2,-3,4').should.throw "negatives not allowed"

	it 'should return ignore values greater than 1000', ->
		(Calc.add '999,1000,1001').should.equal(1999)


# end of tests