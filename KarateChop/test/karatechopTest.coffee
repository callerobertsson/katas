chai = require 'chai'
chai.should()

Chop = require '../src/karatechop'

describe 'Karate Chop', ->
	
	it 'should work correctly', ->
		(Chop.chop 3, []).should.equal -1
		(Chop.chop 3, [1]).should.equal -1
		(Chop.chop 1, [1]).should.equal 0

		(Chop.chop 1, [1, 3, 5]).should.equal 0
		(Chop.chop 3, [1, 3, 5]).should.equal 1
		(Chop.chop 5, [1, 3, 5]).should.equal 2
		(Chop.chop 0, [1, 3, 5]).should.equal -1
		(Chop.chop 2, [1, 3, 5]).should.equal -1
		(Chop.chop 4, [1, 3, 5]).should.equal -1
		(Chop.chop 6, [1, 3, 5]).should.equal -1

		(Chop.chop 1, [1, 3, 5, 7]).should.equal 0
		(Chop.chop 3, [1, 3, 5, 7]).should.equal 1
		(Chop.chop 5, [1, 3, 5, 7]).should.equal 2
		(Chop.chop 7, [1, 3, 5, 7]).should.equal 3
		(Chop.chop 0, [1, 3, 5, 7]).should.equal -1
		(Chop.chop 2, [1, 3, 5, 7]).should.equal -1
		(Chop.chop 4, [1, 3, 5, 7]).should.equal -1
		(Chop.chop 6, [1, 3, 5, 7]).should.equal -1
		(Chop.chop 8, [1, 3, 5, 7]).should.equal -1

# end of tests