###
Karate Chop Kata
  http://codekata.pragprog.com/2007/01/kata_two_karate.html
by calle@upset.se

Implementation of binary chop search
###

firstChop = (n, ns) ->

	return -1 if ns.length == 0

	x = 0
	y = ns.length - 1

	# |0|1|2|3|4|
	# |1|3|5|6|9|

	#console.log '\nns: ' + JSON.stringify(ns) + '. n:' + n

	i = 4
	while i > 0 and x isnt y
		i--

		m =  x + parseInt (y - x) / 2

		console.log "  x: #{x}, y: #{y}, m: #{m}, ns[m]: #{ns[m]}"

		return m if ns[m] == n

		y = m if ns[m] > n

		x = m + 1 if ns[m] < n

	return x if ns[x] is n

	-1


exports.chop = firstChop

		

